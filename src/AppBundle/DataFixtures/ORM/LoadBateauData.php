<?php
/**
 * Created by PhpStorm.
 * User: axel
 * Date: 26/03/18
 * Time: 10:03
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Bateau;
use AppBundle\Entity\Media;
use AppBundle\Entity\Temoignage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadBateauData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /* navire 1 */
        $navire1 = new Bateau();
        $navire1->setNom("France 1");
        $navire1->setHistoire("Le France 1 a assuré ses missions pendant 27 années jusqu\'à l\'entrée en service des satellites météorologiques en 1985. Il devait alors opérer de longues stations sur les lieux de formation et de passage des pires dépressions au large de l\'océan Atlantique. Fouetté par des vents atteignant parfois 100 noeuds (180km/h), balancé par des creux de 20 mètres, ce navire, quel que soit le temps, a rempli sa mission pendant près de 30 ans, et a fait la pluie et le beau temps pour la Météorologie Nationale.");
        $navire1->setType("Navire Météorologique Stationnaire");
        $navire1->setAnnee(1958);
        $navire1->setMateriaux("Acier");
        $navire1->setConstructeur("Forges et Chantiers de la Méditerranée");
        $navire1->setLongueur(76.20);
        $navire1->setLargeur(12.50);
        $navire1->setTirant(4.40);
        $navire1->setProprietaire("Ville de la Rochelle");
        $navire1->setPrixAchat(1200000);
        $navire1->setLatitude(46.154675);
        $navire1->setLongitude(-1.152656);

        $manager->persist($navire1);
        $manager->flush();


        /* navire 2 */
        $navire2 = new Bateau();
        $navire2->setNom("Angoumois");
        $navire2->setHistoire("Construit aux Ateliers et Chantiers de Dieppe pour l’Association Rochelaise de Pêche à Vapeur (ARPV) en 1969. Il débarquait entre 25 et 50 tonnes de poissons pour des campagnes de pêche d’une durée moyenne de 12 jours. (total année 1989 : 620 tonnes débarquées). En avril 1991, une panne moteur l’immobilise définitivement à quai.");
        $navire2->setType("Navire de pêche");
        $navire2->setAnnee(1969);
        $navire2->setMateriaux("Acier");
        $navire2->setConstructeur("Forges et Chantiers de Dieppe");
        $navire2->setLongueur(38.20);
        $navire2->setLargeur(8.35);
        $navire2->setTirant(4.40);
        $navire2->setProprietaire("Arpv(Association Rochelaise de Peche a Vapeur");
        $navire2->setPrixAchat(null);
        $navire2->setLatitude(46.151199);
        $navire2->setLongitude(-1.151543);

        $manager->persist($navire2);
        $manager->flush();

        /* navire 3 */
        $navire3 = new Bateau();
        $navire3->setNom("Saint Gilles");
        $navire3->setHistoire("Construit en 1958 aux chantiers navals des ACRP à La Rochelle, ce navire termine sa carrière au port de La Pallice où il est désarmé en 1989. Le président de l\'Union des Remorqueurs de l`Océan demande alors au Musée Maritime de La Rochelle d`en assurer la conservation. Le remorqueur est hissé sur le slipway en septembre 1994 et après plusieurs phases de restauration a été remis à l`eau le 18 septembre 2009.");
        $navire3->setType("Remorqueur portuaire et de haute mer");
        $navire3->setAnnee(1958);
        $navire3->setMateriaux("Acier");
        $navire3->setConstructeur("Forges et Chantiers de La Rochelle Pallice");
        $navire3->setLongueur(30.30);
        $navire3->setLargeur(7.92);
        $navire3->setTirant(3.75);
        $navire3->setProprietaire("Union des remorqueurs de l`ocean");
        $navire3->setPrixAchat(150000);
        $navire3->setLatitude(46.151261);
        $navire3->setLongitude(-1.151403);

        $manager->persist($navire3);
        $manager->flush();

        /* navire 4 */
        $navire4 = new Bateau();
        $navire4->setNom("Joshua");
        $navire4->setHistoire("Ketch rouge mythique, derrière lequel se dessine en filigrane la silhouette de Bernard Moitessier, grand écrivain de mer et premier homme à avoir eu dans son sillage un tour du monde et demi sur les mers du globe, en solitaire et sans escale. Ce voilier en acier de 12 mètres a été baptisé Joshua en hommage au célèbre navigateur Joshua Slocum.");
        $navire4->setType("Plaisance, voilier, ketch");
        $navire4->setAnnee(1962);
        $navire4->setMateriaux("Acier");
        $navire4->setConstructeur("Méta (JEAN KNOCKER)");
        $navire4->setLongueur(16.00);
        $navire4->setLargeur(3.68);
        $navire4->setTirant(1.60);
        $navire4->setProprietaire("Bernard Moitessier");
        $navire4->setPrixAchat(245000);
        $navire4->setLatitude(46.151199);
        $navire4->setLongitude(-1.151543);

        $manager->persist($navire4);
        $manager->flush();

        /* navire5  */
        $navire5 = new Bateau();
        $navire5->setNom("Capitaine de frégate le verger");
        $navire5->setHistoire("Le 1 er Juin 1992, l`escorteur d`escadre Duperré, le plus ancien des bâtiment de combat de la Marine prenait sa retraite après 36 ans de bons et loyaux services, et après avoir parcouru plus de 700 000 milles durant plus de 55 000 heures. La Marine Nationale cède alors la vedette de ce bâtiment d`escadre à La Rochelle.'");
        $navire5->setType("Canot Major Duperré");
        $navire5->setAnnee(1954);
        $navire5->setMateriaux("Bois");
        $navire5->setConstructeur(null);
        $navire5->setLongueur(null);
        $navire5->setLargeur(null);
        $navire5->setTirant(null);
        $navire5->setProprietaire("SNSM");
        $navire5->setPrixAchat(null);
        $navire5->setLatitude(46.154641);
        $navire5->setLongitude(-1.152672);

        $manager->persist($navire5);
        $manager->flush();

        /* navire6  */
        $navire6 = new Bateau();
        $navire6->setNom("Vedette Duperre");
        $navire6->setHistoire("Les Sociétés de Sauvetage en Mer sont le symbole vivant de la solidarité des gens de mer, de leur abnégation, de leur courage, de leur dévouement. Dès sa création, le Musée Maritime de La Rochelle a eu à coeur de doter sa flotte patrimoniale d’un canot de la SNSM. A son arrivée à La Rochelle, l’équipe du Musée Maritime procède à un grand carénage. Le bateau est sorti de l’eau devant le France I, il est entièrement repeint et sa mécanique révisée.Il navigue très souvent pour participer à la sécurité des régates patrimoniales, embarquer le comité, ou participer à des hommages à la SNSM et à des manifestations nautiques.");
        $navire6->setType("Canot de sauvetage tous temps");
        $navire6->setAnnee(1954);
        $navire6->setMateriaux("Acajou, cuivre");
        $navire6->setConstructeur("Lemaistre Fécamp");
        $navire6->setLongueur(14.40);
        $navire6->setLargeur(4.25);
        $navire6->setTirant(1.40);
        $navire6->setProprietaire("Marinne Nationnale");
        $navire6->setPrixAchat(null);
        $navire6->setLatitude(46.151199);
        $navire6->setLongitude(-1.151543);

        $manager->persist($navire6);
        $manager->flush();

        /* navire7  */
        $navire7 = new Bateau();
        $navire7->setNom("Drague a vapeur TD 6");
        $navire7->setHistoire("Construit en 1955 à l`emplacement actuelle du Musée Maritime de La Rochelle, le Manuel Joël a pêché au large dans les eaux du Golfe de Gascogne jusqu`en 1979, puis a pratiqué la navigation côtière au large des Pertuis charentais. En 1992, après 37 années de navigation, son propriétaire, Henri Teillet, en fait don au Musée Maritime de La Rochelle. Le chalutier rejoint ainsi les unités du Musée Maritime de La Rochelle qui le fait classer au titre de Monument Historique en septembre 1994. Ce bateau est l`un des derniers témoins des chalutiers pêche latérale, dits \"classiques\".");
        $navire7->setType("Chalutier");
        $navire7->setAnnee(1954);
        $navire7->setMateriaux("Bois de chêne");
        $navire7->setConstructeur("Union Sablaise La Rochelle");
        $navire7->setLongueur(14.40);
        $navire7->setLargeur(5.85);
        $navire7->setTirant(3.00);
        $navire7->setProprietaire("Musee Maritime");
        $navire7->setPrixAchat(null);
        $navire7->setLatitude(46.151199);
        $navire7->setLongitude(-1.151543);

        $manager->persist($navire7);
        $manager->flush();

        /* navire8  */
        $navire8 = new Bateau();
        $navire8->setNom("Manuel Joël");
        $navire8->setHistoire("Cet énorme bâtiment aidera à débarrasser le port de La Rochelle de nombreuses épaves qui l’encombrent. Capable d’extraire 400 m3 de vase par jour à une profondeur de 5.50 m, cet engin mesure 28,50 m de long. N’ayant pas de moteur de propulsion, La drague est remorquée par les porteurs de vase Saint-Marc et Bout Blanc. La carrière de D6 s’achève fin 1987. Elle est alors vouée à la casse. L’association de sauvegarde « TD6 », présidée par le fondateur du Musée Maritime Patrick Schnepp décide de sauver la Drague à vapeur de la ferraille. Elle est classée au titre de Monuments Historiques depuis novembre 1992.");
        $navire8->setType("Drague portuaire");
        $navire8->setAnnee(1906);
        $navire8->setMateriaux("Acier");
        $navire8->setConstructeur("null");
        $navire8->setLongueur(42.00);
        $navire8->setLargeur(10.00);
        $navire8->setTirant(1.85);
        $navire8->setProprietaire("Armement Henri Teillet");
        $navire8->setPrixAchat(null);
        $navire8->setLatitude(46.151083);
        $navire8->setLongitude(-1.152172);

        $manager->persist($navire8);
        $manager->flush();


        $image1 = new Media();
        $image1->setUrl("http://127.0.0.1:8000/assets/images/france1.jpg");


        $manager->persist($image1);
        $image1->setBateau($navire1);
        $manager->flush();

        $image2 = new Media();
        $image2->setBateau($navire2);
        $image2->setUrl("http://127.0.0.1:8000/assets/images/angoumois.jpg");

        $manager->persist($image2);
        $manager->flush();

        $image3 = new Media();
        $image3->setBateau($navire3);
        $image3->setUrl("http://127.0.0.1:8000/assets/images/stgilles.jpg");

        $manager->persist($image3);
        $manager->flush();

        $image4 = new Media();
        $image4->setBateau($navire4);
        $image4->setUrl("http://127.0.0.1:8000/assets/images/joshua.jpg");

        $manager->persist($image4);
        $manager->flush();

        $image5 = new Media();
        $image5->setBateau($navire5);
        $image5->setUrl("http://127.0.0.1:8000/assets/images/vduperre.jpg");

        $manager->persist($image5);
        $manager->flush();

        $image6 = new Media();
        $image6->setBateau($navire6);
        $image6->setUrl("http://127.0.0.1:8000/assets/images/cptFregateVerger.jpg");

        $manager->persist($image6);
        $manager->flush();

        $image7 = new Media();
        $image7->setBateau($navire7);
        $image7->setUrl("http://127.0.0.1:8000/assets/images/manuelJoel.jpg");

        $manager->persist($image7);
        $manager->flush();

        $image8 = new Media();
        $image8->setBateau($navire8);
        $image8->setUrl("http://127.0.0.1:8000/assets/images/dragVapeur.jpg");

        $manager->persist($image8);
        $manager->flush();


        $temoignage1 = new Temoignage();
        $temoignage1->setNom("Benec'h");
        $temoignage1->setPrenom("Jean-Paul");
        $temoignage1->setTemoignage("La navigationLancer de ballons sonde, de nuit, à bord du France 1  à bord de ces frégates météo était très différente du travail à bord des autres navires de la marine marchande. Pour les équipages de Delmas-Vieljeux, l’armement rochelais, cela n’avait rien à voir avec le travail à bord d’un navire qui faisait de longues traversées et qui devait effectuer les manœuvres aux escales, charger et décharger. Certains marins appréciaient ce rythme plus tranquille. Par contre les météorologistes, eux, avaient un emploi du temps contraignant avec des quarts d’observation, des relevés, des lâchers de ballon à assurer de nuit comme de jour, vingt-quatre heures sur vingt-quatre par tous les temps et par toutes les mers. La vie à bord était tributaire des conditions de mer et parfois, cela devait être extrêmement pénible. Je n’ai pas connu ces conditions à l’époque, parce que c’était l’été. Il a fait très beau, avec un peu de mauvais temps en passant l’Irlande, mais j’ai entendu des témoignages de collègues qui ont connu du « dur ».");
        $temoignage1->setBateau($navire1);

        $manager->persist($temoignage1);
        $manager->flush();

        $temoignage2 = new Temoignage();
        $temoignage2->setNom("Brand");
        $temoignage2->setPrenom("Yves");
        $temoignage2->setTemoignage("Sur ces bateaux, il y avait un état-major et un équipage complet de la marine marchande d’une trentaine de personnes, ainsi qu’une dizaine de techniciens de la météo. L’avantage, sur ces bateaux, c’est qu’il y avait un médecin de la marine nationale qui était embarqué comme le bateau restait près d’un mois en mer sans pouvoir revenir. En fait, le médecin n’était pas tellement embarqué pour les gens du bord, c’était surtout en cas d’assistance à des blessés sur des bateaux de pêche, des voiliers ou sur d’autres navires. Il y avait en permanence un système de quart comme sur tous les navires. Les gens par roulement s’occupaient de maintenir la position du navire à l’endroit désiré. Au début, il était convenu, comme sur tous les navires, que le commandant n’était pas astreint à faire de quart car il devait être disponible 24h/24 donc. Par la suite, avec les réductions d’effectif, il a été décidé ou jugé que le commandant pouvait assurer un quart. Et c’est à ce moment que j’ai décidé de quitter les frégates. Je ne trouvais pas cela normal surtout pour une question de principe. Par beau temps, pas de problème, on peut assurer un quart de 4h ou 8h par jour. Mais en cas de mauvais temps, le commandant passait 24 ou 48h, tout le temps de la tempête, sur la passerelle. Donc il ne fallait pas être astreint en plus à un quart. L’équipage a des appellations pour désigner le commandant :  le pacha, le vieux, le tonton, tout ça ce sont des mots gentils. Il y avait peut-être aussi des surnoms qui n’étaient pas dévoilés, je ne sais pas…sans être très distant, il fallait quand même que le commandant ait un minimum d’autorité. En dernier ressort, la décision finale appartient au commandant. Il peut prendre des conseils auprès d’autres personnes du bord. Cela s’appelle d’ailleurs la « réunion des principaux de l’équipage ». C’est un grand mot ! En cas de décision importante, le commandant se doit de poser la question au chef mécanicien par exemple. Sur les navires météo, on consultait aussi le chef météo, ou le maître d’équipage qui avait aussi son expérience pratique. En tout état de cause, la décision finale, reviendra toujours au commandant. Et si la décision n’est pas bonne, c’est à lui qu’en incombera la responsabilité pas aux les gens qui l’auront conseillé !  Le rôle principal du commandant consiste à réduire les tensions à bord surtout lorsqu’il y a des groupes un peu différents. Les météo faisaient un peu bande à part, l’équipage aussi…néanmoins, toutes les occasions étaient bonnes pour se retrouver : les fêtes, les anniversaires, les mi-points.");
        $temoignage2->setBateau($navire1);

        $manager->persist($temoignage2);
        $manager->flush();
    }


}
