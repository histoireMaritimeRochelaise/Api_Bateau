<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bateau
 *
 * @ORM\Table(name="bateau")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BateauRepository")
 */
class Bateau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="histoire", type="text")
     */
    private $histoire;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="annee", type="integer")
     */
    private $annee;

    /**
     * @var string
     *
     * @ORM\Column(name="materiaux", type="string", length=255)
     */
    private $materiaux;

    /**
     * @var string
     *
     * @ORM\Column(name="constructeur", type="string", length=255, nullable=true)
     */
    private $constructeur;

    /**
     * @var float
     *
     * @ORM\Column(name="longueur", type="float", nullable=true)
     */
    private $longueur;

    /**
     * @var float
     *
     * @ORM\Column(name="largeur", type="float", nullable=true)
     */
    private $largeur;

    /**
     * @var float
     *
     * @ORM\Column(name="tirant", type="float", nullable=true)
     */
    private $tirant;

    /**
     * @var string
     *
     * @ORM\Column(name="proprietaire", type="string", length=255, nullable=true)
     */
    private $proprietaire;

    /**
     * @var float
     *
     * @ORM\Column(name="prixAchat", type="float", nullable=true)
     */
    private $prixAchat;

    /**
     * @var float
     *
     * @ORM\Column(name="latitute", type="float")
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float")
     */
    private $longitude;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Bateau
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set histoire
     *
     * @param string $histoire
     *
     * @return Bateau
     */
    public function setHistoire($histoire)
    {
        $this->histoire = $histoire;

        return $this;
    }

    /**
     * Get histoire
     *
     * @return string
     */
    public function getHistoire()
    {
        return $this->histoire;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Bateau
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     *
     * @return Bateau
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return int
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set materiaux
     *
     * @param string $materiaux
     *
     * @return Bateau
     */
    public function setMateriaux($materiaux)
    {
        $this->materiaux = $materiaux;

        return $this;
    }

    /**
     * Get materiaux
     *
     * @return string
     */
    public function getMateriaux()
    {
        return $this->materiaux;
    }

    /**
     * Set constructeur
     *
     * @param string $constructeur
     *
     * @return Bateau
     */
    public function setConstructeur($constructeur)
    {
        $this->constructeur = $constructeur;

        return $this;
    }

    /**
     * Get constructeur
     *
     * @return string
     */
    public function getConstructeur()
    {
        return $this->constructeur;
    }

    /**
     * Set longueur
     *
     * @param float $longueur
     *
     * @return Bateau
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Get longueur
     *
     * @return float
     */
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * Set largeur
     *
     * @param float $largeur
     *
     * @return Bateau
     */
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Get largeur
     *
     * @return float
     */
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * Set tirant
     *
     * @param float $tirant
     *
     * @return Bateau
     */
    public function setTirant($tirant)
    {
        $this->tirant = $tirant;

        return $this;
    }

    /**
     * Get tirant
     *
     * @return float
     */
    public function getTirant()
    {
        return $this->tirant;
    }

    /**
     * Set proprietaire
     *
     * @param string $proprietaire
     *
     * @return Bateau
     */
    public function setProprietaire($proprietaire)
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

    /**
     * Get proprietaire
     *
     * @return string
     */
    public function getProprietaire()
    {
        return $this->proprietaire;
    }

    /**
     * Set prixAchat
     *
     * @param float $prixAchat
     *
     * @return Bateau
     */
    public function setPrixAchat($prixAchat)
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    /**
     * Get prixAchat
     *
     * @return float
     */
    public function getPrixAchat()
    {
        return $this->prixAchat;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Bateau
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Bateau
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}
