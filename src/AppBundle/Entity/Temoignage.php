<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Temoignage
 *
 * @ORM\Table(name="temoignage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TemoignageRepository")
 */
class Temoignage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="temoignage", type="text")
     */
    private $temoignage;


    /**
    *@ORM\ManyToOne(targetEntity="AppBundle\Entity\Bateau")
    *@ORM\JoinColumn(nullable=false)
    */
    private $bateau;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Temoignage
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Temoignage
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set temoignage
     *
     * @param string $temoignage
     *
     * @return Temoignage
     */
    public function setTemoignage($temoignage)
    {
        $this->temoignage = $temoignage;

        return $this;
    }

    /**
     * Get temoignage
     *
     * @return string
     */
    public function getTemoignage()
    {
        return $this->temoignage;
    }

    /**
     * Set bateau
     *
     * @param \AppBundle\Entity\Bateau $bateau
     *
     * @return Temoignage
     */
    public function setBateau($bateau)
    {
        $this->bateau = $bateau;

        return $this;
    }

    /**
     * Get bateau
     *
     * @return \AppBundle\Entity\Bateau
     */
    public function getBateau()
    {
        return $this->bateau;
    }


}
