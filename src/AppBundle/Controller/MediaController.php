<?php
/**
 * Created by PhpStorm.
 * User: axel
 * Date: 28/03/18
 * Time: 16:12
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Media;
use AppBundle\Entity\Bateau;

class MediaController extends Controller
{

    public function getMediasAction(Request $request)
    {
        $medias = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Media')
            ->findAll();
        /* @var $medias Media[] */

        $formatted = [];
        foreach ($medias as $media) {
            $formatted[] = [
                'id' => $media->getId(),
                'Url' => $media->getUrl(),
                'Bateau_associe' => $media->getBateau(),
            ];
        }

        return new JsonResponse($formatted);
    }



    public function getMediaAction($id, Request $request)
    {
        $media = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Media')
            ->find($id);
        /* @var $media Media */

        $formatted = [
            'id' => $media->getId(),
            'Url' => $media->getUrl(),
            'Bateau_associé' => $media->getBateau(),
        ];

        if (empty($media)) {
            return new JsonResponse(['message' => 'Media not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($formatted);
    }
}
