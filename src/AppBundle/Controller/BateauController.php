<?php
/**
 * Created by PhpStorm.
 * User: axel
 * Date: 28/03/18
 * Time: 16:12
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Bateau;

class BateauController extends Controller
{

    public function getBateauxAction(Request $request)
    {
        $bateaux = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Bateau')
            ->findAll();
        /* @var $bateaux Bateau[] */

        $formatted = [];
        foreach ($bateaux as $bateau) {
            $formatted[] = [
                'id' => $bateau->getId(),
                'Nom' => $bateau->getNom(),
                'Histoire' => $bateau->getHistoire(),
                'Type' => $bateau->getType(),
                'Annee' => $bateau->getAnnee(),
                'Materiaux' => $bateau->getMateriaux(),
                'Constructeur' => $bateau->getConstructeur(),
                'Longueur' => $bateau->getLongueur(),
                'Largeur' => $bateau->getLargeur(),
                'Tirant' => $bateau->getTirant(),
                'Propriétaire' => $bateau->getProprietaire(),
                'Prix_Achat' => $bateau->getPrixAchat(),
                'Latitude' => $bateau->getLatitude(),
                'Longitude' => $bateau->getLongitude(),

            ];
        }

        return new JsonResponse($formatted);
    }



    public function getBateauAction($id, Request $request)
    {
        $bateau = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Bateau')
            ->find($id);
        /* @var $bateau Bateau */



        $formatted = [
            'id' => $bateau->getId(),
            'Nom' => $bateau->getNom(),
            'Histoire' => $bateau->getHistoire(),
            'Type' => $bateau->getType(),
            'Annee' => $bateau->getAnnee(),
            'Materiaux' => $bateau->getMateriaux(),
            'Constructeur' => $bateau->getConstructeur(),
            'Longueur' => $bateau->getLongueur(),
            'Largeur' => $bateau->getLargeur(),
            'Tirant' => $bateau->getTirant(),
            'Propriétaire' => $bateau->getProprietaire(),
            'Prix_Achat' => $bateau->getPrixAchat(),
            'Latitude' => $bateau->getLatitude(),
            'Longitude' => $bateau->getLongitude(),
        ];

        if (empty($bateau)) {
            return new JsonResponse(['message' => 'Bateau not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($formatted);
    }
}