<?php
/**
 * Created by PhpStorm.
 * User: axel
 * Date: 28/03/18
 * Time: 16:12
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Temoignage;
use AppBundle\Entity\Bateau;

class TemoignageController extends Controller
{

    public function getTemoignagesAction(Request $request)
    {
        $temoignages = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Temoignage')
            ->findAll();
        /* @var $temoignages Temoignage[] */

        $formatted = [];
        foreach ($temoignages as $temoignage) {
            $formatted[] = [
                'id' => $temoignage->getId(),
                'Nom' => $temoignage->getNom(),
                'Temoignage' => $temoignage->getTemoignage(),
                'Bateau_associe' => $temoignage->getBateau(),
            ];
        }

        return new JsonResponse($formatted);
    }



    public function getTemoignageAction($id, Request $request)
    {
        $temoignage = $this->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Temoignage')
            ->find($id);
        /* @var $temoignage Temoignage */

        $formatted = [
            'id' => $temoignage->getId(),
            'Nom' => $temoignage->getNom(),
            'Temoignage' => $temoignage->getTemoignage(),
            'Bateau_associe' => $temoignage->getBateau(),
        ];

        if (empty($temoignage)) {
            return new JsonResponse(['message' => 'Temoignage not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($formatted);
    }
}


